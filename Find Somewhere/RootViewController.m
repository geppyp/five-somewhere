//
//  RootViewController.m
//  Find Somewhere
//
//  Created by Harleen on 1/14/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "MapViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"Five Somewhere";
    
    UIBarButtonItem *favBtn = [[UIBarButtonItem alloc] initWithTitle:@"Favorites" style:UIBarButtonItemStylePlain target:self action:@selector(showFavorites)];
    self.navigationItem.rightBarButtonItem = favBtn;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showFavorites {
    
}

- (IBAction)valueChanged:(id)sender
{
    NSInteger selectedIndex = ((UISegmentedControl*)sender).selectedSegmentIndex;
    if (selectedIndex == 1)
    {
        MapViewController* mapViewController = [[MapViewController alloc]initWithNibName:@"MapViewController" bundle:nil] ;
        
        // Can't add MapViewController as a child view controller, as RootViewController is not a container view controller.
        //[self addChildViewController:mapViewController];
        
        // TODO: This is obvioulsy wrong...
        UIView* mapView = mapViewController.view;
        mapViewController.view = nil;
        self.view = mapView;
        
    }
}
@end
