//
//  MapViewController.h
//  Find Somewhere
//
//  Created by Aaron Zielinski on 1/14/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
