//
//  AppDelegate.h
//  Find Somewhere
//
//  Created by Geppy Parziale on 1/14/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//  test

#import <UIKit/UIKit.h> 

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
